var counterchat = 0;

$(function () {
	$("#tutup-arrow").hide();
	$("#atasnya-chat-body").hide();

  $("textarea").keypress(function(event) {
    if (event.which == 13){
    counterchat++;
  	var date = new Date();
    var newChat = "";
    if (counterchat % 2 == 0){
      newChat = "<div class=\"msg-send\">" + $("textarea").val() + " " + date.getHours() + ":" + date.getMinutes() + "</div>";  
    } else {
      newChat = "<div class=\"msg-receive\">" + $("textarea").val() + " " + date.getHours() + ":" + date.getMinutes() + "</div>";  
    }

  	$("textarea").val("").focus();
  	var prevChat = $(".msg-insert").html();
  	$(".msg-insert").html(prevChat + newChat);
  	$("textarea").focus(); 	
  }
  });

    $("#tutup-arrow").click(function(event){
    	$("#atasnya-chat-body").hide();
    	$("#tutup-arrow").hide();
    	$("#buka-arrow").show();
  
    });
    $("#buka-arrow").click(function(event){

  		$("#atasnya-chat-body").show();
    	$("#tutup-arrow").show();
    	$("#buka-arrow").hide();
      $("textarea").focus();    	
    });

});



function go(x){
var print = document.getElementById('print');
var erase = false;
if (x === 'ac') {
    /* implemetnasi clear all */
    $("#print").val('');
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}


var themes = [
{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

localStorage.themes = JSON.stringify(themes);

$(document).ready(function() {
    // buat select 
    $('.my-select').select2();
    $('.my-select').select2({
    'data': JSON.parse(localStorage.themes)
    });
    //apply button 
    $('.apply-button-class').on('click', function(){  // sesuaikan class button
      // [TODO] ambil value dari elemen select .my-select
      var id_themes = $(".my-select").val();
      console.log("memilik id theme " + id_themes);
      // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
      
      // parsing json 
      var themes_dipilih = JSON.parse(localStorage.themes)[id_themes];

      console.log("memilih " + themes_dipilih["text"]);
      // [TODO] ambil object theme yang dipilih
      
      // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
      $("body").css('background-color', themes_dipilih["bcgColor"]);
      $("body").css('color', themes_dipilih["fontColor"]);
      // [TODO] simpan object theme tadi ke local storage selectedTheme
      var warna = themes_dipilih["text"];
      var bcg = themes_dipilih["bcgColor"];
      var font = themes_dipilih["fontColor"];
      var obj_mau_disimpan_stringified = "{\""+warna+"\":{\"bcgColor\":\"" + bcg + "\",\"fontColor\":\"" + font + "\"}}";
      
      localStorage.selectedThemes = obj_mau_disimpan_stringified;
      localStorage.namatema = warna;
      console.log(localStorage.namatema + " sebagai nama tema");
      console.log("menyimpan " + obj_mau_disimpan_stringified + "ke localStorage");
    })


    var obj_tema_sekarang;
    if (localStorage.selectedThemes) {
      //localStorage.selectedThemes sudah ada isinya, tinggal parsing dari yang sudah ada 
      obj_tema_sekarang = JSON.parse(localStorage.selectedThemes);
      var nama = localStorage.namatema;
      $("body").css('background-color', obj_tema_sekarang[nama].bcgColor);
      $("body").css('color', obj_tema_sekarang[nama].fontColor);
    } else {
      //localStorage.selectedThemes belum ada, bikin baru dari default 
      var selectedThemes = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
      obj_tema_sekarang = selectedThemes;
      //simpan menjadi tema sekarang
      localStorage.selectedThemes = JSON.stringify(obj_tema_sekarang);
      var nama = "Indigo";
      localStorage.namatema = nama;
      $("body").css('background-color', obj_tema_sekarang[nama].bcgColor);
      $("body").css('color', obj_tema_sekarang[nama].fontColor);
    }
    console.log("tema sekarang " + localStorage.selectedThemes);
    
    //set tema hehe
   

    

});